# _*_ coding=utf-8 _*_
import tensorflow as tf

w = tf.Variable(tf.random_normal([2, 3], stddev=2, mean=0, seed=1))

# tf.truncated_normal() #去掉过大分离点的正态分布
# tf.random_uniform() #平均分布

print(tf.zeros([3, 2]))
print(tf.ones([3, 2]))
print(tf.fill([3, 2], 6))
print(tf.constant([3, 2, 1]))
