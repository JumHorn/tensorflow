# coding:utf-8
import tensorflow as tf
import numpy as np
BATCH_SIZE = 8
SEED = 23455

rdm = np.random.RandomState(SEED)
X = rdm.rand(32, 2)
Y = [[x1+x2+(rdm.rand()/10-0.05)] for (x1, x2) in X]
print("X:\n", X)
print("Y:\n", Y)

# 定义神经网络的输入 参数和输出 定义前向传播过程
x = tf.placeholder(tf.float32, shape=(None, 2))
y_ = tf.placeholder(tf.float32, shape=(None, 1))

w1 = tf.Variable(tf.random_normal([2, 1], stddev=1, seed=1))
w2 = tf.Variable(tf.random_normal([3, 1], stddev=1, seed=1))

a = tf.matmul(x, w1)
y = tf.matmul(a, w2)

# 定义损失函数以及反向传播方法
loss = tf.reduce_mean(tf.square(y-y_))
tran_step = tf.train.GradientDescentOptimizer(0.001).minimize(loss)
# tran_step=tf.train.MomentumOptimizer(0.001,0.9).minimize(loss)
# tran_step=tf.train.AdagradDAOptimizer(0.001).minimize(loss)

# 生成会话 训练
with tf.Session() as sess:
    init_op = tf.global_variables_initializer()
    sess.run(init_op)
    # 输出目标(未经训练)的参数取值
    print("w1:\n", sess.run(w1))
    print("w2:\n", sess.run(w2))
    print("\n")

    STEPS = 3000
    for i in range(STEPS):
        start = (i*BATCH_SIZE) % 32
        end = start+BATCH_SIZE
        sess.run(tran_step, feed_dict={x: X[start: end], y_: Y[start: end]})
        if i % 500 == 0:
            total_loss = sess.run(loss, feed_dict={x: X, y_: Y})
            print("After %d training steps,loss on all data is %g" %
                  (i, total_loss))

    print("\n")
    print("w1:\n", sess.run(w1))
    print("w2:\n", sess.run(w2))
